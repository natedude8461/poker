
var Suits = 'Hearts Spades Diamonds Clubs'.split(' ')
  , Ranks = 'Two Three Four Five Six Seven Eight Nine Ten Jack Queen King Ace'.split(' ')
  , Hands = 'HighCard Pair TwoPair ThreeOfAKind Straight Flush FullHouse FourOfAKind StraightFlush RoyalFlush'.split(' ')
  ;

function Card(suit, rank) {
	if (Suits.indexOf(suit) < 0) throw new Error('suit is invalid');
	this.suit = suit;

	if (Ranks.indexOf(rank) < 0) throw new Error('rank is invalid');
	this.rank = rank;
}
Card.prototype.equals = function(c) {
	return this.suit == c.suit && this.rank == c.rank;
};

function Deck() {
	var cards = this.cards = [];
	Suits.forEach(function(s) {
		Ranks.forEach(function(r) {
			cards.push(new Card(s, r));
		});
	});
}
Deck.prototype.shuffle = function() {
	var length = this.cards.length;
	for (var i=0; i<length; i++) {
		var index = Math.floor(Math.random()*(length-1));
		var temp = this.cards[index];
		this.cards[index] = this.cards[i];
		this.cards[i] = temp;
	}
};
Deck.prototype.draw = function(fn) {
	return this.cards.shift();
};

function Hand() {
	this.cards = [];
}
Hand.prototype.add = function(card) {
	this.cards.push(card);
	this.cards.sort(function(a,b) {
		return Ranks.indexOf(a.rank) - Ranks.indexOf(b.rank);
	});
};
Hand.prototype.size = function() {
	return this.cards.length;
};
Hand.prototype.getCountsBySuit = function() {
	var countsBySuit = {};
	Suits.forEach(function(suit) { countsBySuit[suit] = 0; });
	this.cards.forEach(function(card) {
		countsBySuit[card.suit]++;
	});
	return countsBySuit;
};
Hand.prototype.getCountsByRank = function() {
	var countsByRank = {};
	Ranks.forEach(function(rank) { countsByRank[rank] = 0; });
	this.cards.forEach(function(card) {
		countsByRank[card.rank]++;
	});
	return countsByRank;
};
Hand.prototype.getNumberOfPairs = function() {
	var numberOfPairs = 0
	  , countsByRank = this.getCountsByRank()
	  ;
	for (var k in countsByRank) {
		if (countsByRank[k] > 1) {
			numberOfPairs++;
		}
	}
	return numberOfPairs;
};
Hand.prototype.getSortedRankIndicies = function() {
	var countsByRank = this.getCountsByRank()
	  , indicies = [];
	for (var k in countsByRank) {
		if (countsByRank[k] > 0)
			indicies.push(Ranks.indexOf(k));
	}
	return indicies;
};
Hand.prototype.hasAce = function() {
	var sortedRankIndicies = this.getSortedRankIndicies();
	var high = sortedRankIndicies.pop();
	if (!high) return false;
	return high == Ranks.indexOf('Ace');
};
Hand.prototype.mightHaveAStraight = function() {
	var sortedRankIndicies = this.getSortedRankIndicies()
	  , highest = sortedRankIndicies[sortedRankIndicies.length - 1 ];
	  return (highest > Ranks.indexOf('Five') && highest <= Ranks.indexOf('Ace'));
};
Hand.prototype.getExpectedStraightOrder = function() {
	var sortedRankIndicies = this.getSortedRankIndicies()
	  , highest = sortedRankIndicies[sortedRankIndicies.length - 1 ]
	  , secondHighest = sortedRankIndicies[sortedRankIndicies.length - 2 ];
	if (highest == Ranks.indexOf('Ace')) {
		if (secondHighest == Ranks.indexOf('Five')) {
			return this.getLowStraightOrder();
		}
		else {
			return this.getHighStraightOrder();
		}
	}
	
	var expected = [];
	for (var i = 0; i<5; i++) {
		expected.unshift(highest - i);
	}
	return expected;
};
Hand.prototype.getCardsByRank = function(rank) {
	return this.getCardsByRankIndex( Ranks.indexOf(rank) );
};
Hand.prototype.getCardsByRankIndex = function(i) {
	var cards = [];
	this.cards.forEach(function(card) {
		if (Ranks.indexOf(card.rank) == i)
			cards.push(card);
	});
	return cards;
};
Hand.prototype.getCardsBySuit = function(suit) {
	return this.getCardsBySuitIndex( Suits.indexOf(suit) );
};
Hand.prototype.getCardsBySuitIndex = function(i) {
	var cards = [];
	this.cards.forEach(function(card) {
		if (Suits.indexOf(card.suit) == i)
			cards.push(card);
	});
	return cards;
};
Hand.prototype.getLowStraightOrder = function() {
	return [ Ranks.indexOf('Two'), Ranks.indexOf('Three'), Ranks.indexOf('Four'), Ranks.indexOf('Five'), Ranks.indexOf('Ace') ];
};
Hand.prototype.getHighStraightOrder = function() {
	return [ Ranks.indexOf('Ten'), Ranks.indexOf('Jack'), Ranks.indexOf('Queen'), Ranks.indexOf('King'), Ranks.indexOf('Ace') ];
};
Hand.prototype.getPairs = function() {
	var cards = [];
	var pairs = [];
	var countsByRank = this.getCountsByRank();
	for (var k in countsByRank) {
		var count = countsByRank[k];
		if (count > 1) {
			var getThisMany = (count - (count % 2));
			this.cards.forEach(function(card) {
				if (card.rank == k ) {
					if (getThisMany-- > 0) {
						cards.push(card);
					}
				}
			});
		}
	}
	var cardsLength = Math.floor(cards.length / 2);
	for (var i=0; i<=cardsLength; i+=2) {
		var pair = [ cards[i], cards[i+1] ];
		pair.getCards = function() {
			return this.slice(0);
		};
		pair.sort(function(a,b) {
			return Suits.indexOf(a.suit) - Suits.indexOf(b.suit);
		});
		pairs.push(pair);
	}
	return pairs;
};
Hand.prototype.getHighCard = function() {
	var highCard = this.cards.slice(this.cards.length - 1);
	if (highCard) {
		highCard.getCards = function() {
			return this.slice(0);
		};
	}
	return highCard;
};
Hand.prototype.getPair = function() {
	return this.getPairs().shift();
};
Hand.prototype.hasPair = function() {
	if (this.size() < 2) return false;
	return this.getPair() != undefined;
};
Hand.prototype.getTwoPair = function() {
	var pairs = this.getPairs();
	if (pairs.length > 1) {
		var twoPair = pairs.slice(0,2);
		if (twoPair) {
			twoPair.getCards = function() {
				return this[0].getCards().concat(this[1].getCards());
			};
		}
	}
	return twoPair;
};
Hand.prototype.hasTwoPair = function() {
	if (this.size() < 4) return false;
	return this.getTwoPair() != undefined;
};
Hand.prototype.getThreeOfAKind = function() { 
	var cards = [];
	var pairs = [];
	var countsByRank = this.getCountsByRank();
	for (var k in countsByRank) {
		var count = countsByRank[k];
		if (count > 2) {
			var getThisMany = (count - (count % 3));
			this.cards.forEach(function(card) {
				if (card.rank == k ) {
					if (getThisMany-- > 0) {
						cards.push(card);
					}
				}
			});
		}
	}
	var cardsLength = Math.floor(cards.length / 3);
	for (var i=0; i<cardsLength; i+=3) {
		pairs.push([ cards[i], cards[i+1], cards[i+2]]);
	}
	var threeOfAKind = pairs.shift();
	if (threeOfAKind) {
		threeOfAKind.getCards = function() {
			return this.slice(0);
		};
	};
	return threeOfAKind;
};
Hand.prototype.hasThreeOfAKind = function() {
	return this.getThreeOfAKind() != undefined;
};
Hand.prototype.getStraight = function() {
	var expectedStraightOrder = this.getExpectedStraightOrder()
	  , sortedRankIndicies = this.getSortedRankIndicies()
	  , cards = []
	  , cardsByRankIndex = null;
	for (var i=0; i<expectedStraightOrder.length; i++) {
		var a = (expectedStraightOrder[i] != sortedRankIndicies[i]);
		if (a)
			return undefined;
		cardsByRankIndex = this.getCardsByRankIndex(sortedRankIndicies[i]);
		if (cardsByRankIndex.length > 0) {
			var card = cardsByRankIndex.shift();
			cards.push(card);
		}
	}
	cards.getCards = function() {
		return this.slice(0);
	};
	return cards;
};
Hand.prototype.hasStraight = function() {
	if (this.size() < 5) return false;
	if (!this.mightHaveAStraight()) return false;
	return this.getStraight() != undefined;
};
Hand.prototype.getFlush = function() {
	var countsBySuit = this.getCountsBySuit();
	var flushes = [];
	for (var k in countsBySuit) {
		if (countsBySuit[k] > 4) {
			flushes.push( this.getCardsBySuit(k) );
		}
	}
	var flush = flushes.shift();
	if (flush) {
		flush.getCards = function() {
			return this.slice(0);
		};
	}
	return flush;
};
Hand.prototype.hasFlush = function() {
	if (this.size() < 5) return false;
	return this.getFlush() != undefined;
};
Hand.prototype.getFullHouse = function() {
	var threeOfAKind = this.getThreeOfAKind();
	if (!threeOfAKind)
		return undefined;
	var pair = this.getPair();
	if (!pair)
		return undefined;
	var fullHouse = [threeOfAKind, pair];
	fullHouse.getCards = function() {
		return threeOfAKind.getCards().concat(pair.getCards());
	};
	return fullHouse;
};
Hand.prototype.hasFullHouse = function() {
	if (this.size() < 5) return false;
	return this.getFullHouse() != undefined;
};
Hand.prototype.getFourOfAKind = function() {
	var twoPair = this.getTwoPair();
	if (!twoPair)
		return undefined;
	var cards = twoPair.getCards();
	var a = (
		(cards[0].rank == cards[1].rank) &&
		(cards[0].rank == cards[2].rank) &&
		(cards[0].rank == cards[3].rank)
	);
	if (!a)
		return undefined;
	return twoPair;
};
Hand.prototype.hasFourOfAKind = function() {
	if (this.size() < 4) return false;
	return this.getFourOfAKind() != undefined;
};
Hand.prototype.getStraightFlush = function() {
	var straight = this.getStraight();
	if (!straight)
		return undefined;
	var cards = straight.getCards();
	var a = (
		(cards[0].suit == cards[1].suit) &&
		(cards[0].suit == cards[2].suit) &&
		(cards[0].suit == cards[3].suit) &&
		(cards[0].suit == cards[4].suit) 
	);
	if (!a)
		return undefined;
	return cards;
};
Hand.prototype.hasStraightFlush = function() {
	if (this.size() < 5) return false;
	return this.getStraightFlush() != undefined;
};
Hand.prototype.getRoyalFlush = function() {
	var straightFlush = this.getStraightFlush();
	if (!straightFlush)
		return undefined;
	var expectedStraightOrder = this.getHighStraightOrder()
	  , sortedRankIndicies = this.getSortedRankIndicies();

	for (var i=0; i<expectedStraightOrder.length; i++) {
		if (expectedStraightOrder[i] != sortedRankIndicies[i])
			return undefined;
	}
	return straightFlush;
};
Hand.prototype.hasRoyalFlush = function() {
	if (this.size() < 5)
		return false;
	if (!this.hasStraightFlush())
		return false;
	return this.getRoyalFlush() != undefined;
};
Hand.prototype.makeBest = function() {
	var cards = undefined;
	var curr;
	for(var i=Hands.length-1; i>=0; i--) {
		if ((curr = this['get'+Hands[i]]()) == undefined)
			continue;

		if (!cards) {
			cards = curr.getCards().slice(0,5);
		}

		var diff = 5 - cards.length;


		var j = 0;
		var tmp = this.cards.reverse();
		while (j < tmp || diff > 0) {

			var c = tmp[j];

			if (cards.indexOf(c) < 0) {
				cards.push(c);
				diff--;
			}
			
			j++;
		}

		break;
	}
	if (cards) {
		cards.getCards = function() {
			return this.slice(0);
		};
	}
	return cards;
};
Hand.prototype.getRank = function() {
	var rank = 0;
	for (var i=1; i<Hands.length; i++) {
		if (!Hand.prototype['has'+Hands[i]].call(this)) 
			break;
		rank = i;
	}
	return Hands[rank];
};
Hand.prototype.getScore = function() {
	var rank = Hands.indexOf(this.getRank());
	var score = rank * 1000;
	var best = this.makeBest();
	best.forEach(function(c) {
		score += Ranks.indexOf(c.rank);
	});
	return score;
};
Hand.prototype.betterThan = function(b) {
	var i = this.getScore(),
		j = b.getScore();
	return i > j;
};

exports.Card = Card;
exports.Deck = Deck;
exports.Hand = Hand;
