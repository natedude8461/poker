var machine = require('./machine');

var id = 0;

var Player = machine.make({
	init: function() {
		console.log('player created');
		this.bank = 1000;
		this.id = (id++);
	},
	initial:'idle',
	transitions: [
		{ event: 'join', from:'idle', to:'joining' },
		{ event: 'join', from:'joining', to:'joined' },
		{ event: 'sit', from:'joined', to:'sitting' },
		{ event: 'sit', from:'sitting', to:'seated' },
		{ event: 'stand', from:'seated', to:'standing' },
		{ event: 'stand', from:'standing', to:'joined' },
		{ event: 'leave', from:'joined', to:'leaving' },
		{ event: 'leave', from:'leaving', to:'idle' },
		{ event: 'wait', from:'seated', to:'waiting' },
		{ event: 'stand', from:'waiting', to:'standing' },
	],
	/*
	transitions: {
		join: [
			{from:'idle', to:'joining'},
			{from:'joining', to:'joined'}
		],
		sit: [
			{from:'joined', to:'sitting'},
			{from:'sitting', to:'seated'}
		],
		stand: [
			{from:'seated', to:'standing'},
			{from:'waiting', to:'standing'},
			{from:'standing', to:'joined'},
		],
		leave: [
			{from:'joined', to:'leaving'},
			{from:'leaving', to:'idle'}
		],
		wait: [
			{from:'seated', to:'waiting'}
		]

	},
	*/
	states: {
		idle: {
			enter: function(p) {
				console.log(p.toString() + ' is idle');
			}
		},
		joining: {
			enter: function(p) {
				console.log(p.toString() + ' is joining');
			}
		},
		joined: {
			enter: function(p) {
				console.log(p.toString() + ' has joined');
			}
		},
		sitting: {
			enter: function(p) {
				console.log(p.toString() + ' is sitting');
			}
		},
		seated: {
			enter: function(p) {
				console.log(p.toString() + ' is seated');
			}
		},
		standing: {
			enter: function(p) {
				console.log(p.toString() + ' is standing');
			}
		},
		leaving: {
			enter: function(p) {
				console.log(p.toString() + ' is leaving');
			}
		},
		waiting: {
			enter: function(p) {
				console.log(p.toString() + ' is waiting');
			}
		}

	}
});
Player.prototype.toString = function() {
	return 'Player#'+this.id;
};

exports.make = function() {
	return new Player();
};

var p = new Player();
p.join();
p.sit();
p.stand();
p.sit();
p.wait();
p.stand();
p.sit();
p.wait();

