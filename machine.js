var EventEmitter = require('events').EventEmitter;

function Machine() {
	EventEmitter.call(this);
	if (this.constructor.config && typeof this.constructor.config.init == 'function')
		this.constructor.config.init.apply(this, Array.prototype.slice.call(arguments));
}
Machine.configure = function(klazz, config) {
	var self = this;
	'getState addState getStates getInitialState getClassEmitter getTransitions getStateTransitions'.split(' ').forEach(function(v) {
		klazz[v] = self[v];
	});
	klazz.config = config;
	if (config.states) {
		for (var k in config.states) {
			var v = config.states[k];
			klazz.addState( new State(k,v) );
		}
	}
	if (config.transitions && config.transitions.forEach) {
		config.transitions.forEach(function(transition) {
			var eventName = transition.event;
			if (!(transition.event in klazz.prototype)) {
				klazz.prototype[transition.event] = function() {
					//the class gets to handle the event first, however "this" inside of the event handler will be the current instance not the class instance
					klazz.getClassEmitter().emit.apply(klazz.getClassEmitter(), [transition.event, this].concat(Array.prototype.slice.call(arguments)));

					//then the individual instance
					this.emit.apply(this, [transition.event].concat(Array.prototype.slice.call(arguments)));
				};
				klazz.getClassEmitter().on(transition.event, function() {
					var args = Array.prototype.slice.call(arguments)
					  , instance = args[0];

					var possible = [];
					config.transitions.forEach(function(transition) {
						console.log(transition);
						if (instance.getCurrentState().name == transition.from || transition.event == eventName)
							possible.push(transition);
					});
					console.log('eventName',eventName,'possible',possible);

					possible.forEach(function(transition) {
						var nextState = klazz.getState(transition.to);
						if (!nextState)
							return;
						if (nextState.evaluate.apply(nextState, args))
							instance.setCurrentState(nextState);
					});
				});
			}
		});
	}
	/*
	if (config.transitions && config.transitions.forEach) {
		config.transitions.forEach(function(transition) {
			if (!(transition.event in klazz.prototype)) {
				klazz.prototype[transition.event] = function() {
					//the class gets to handle the event first, however "this" inside of the event handler will be the current instance not the class instance
					klazz.getClassEmitter().emit.apply(klazz.getClassEmitter(), [transition.event, this].concat(Array.prototype.slice.call(arguments)));

					//then the individual instance
					this.emit.apply(this, [transition.event].concat(Array.prototype.slice.call(arguments)));
				};
			}
			klazz.getClassEmitter().on(transition.event, function() {
				var args = Array.prototype.slice.call(arguments)
				  , instance = args[0];
				if (instance.getCurrentState().name != transition.from)
					return false;
				var nextState = klazz.getState(transition.to);
				if (!nextState)
					return;
				if (nextState.evaluate.apply(nextState, args))
					instance.setCurrentState(nextState);
			});
		});

	}
	*/
	/*
	if (config.transitions) {
		for (var event in config.transitions) {
			var transitions = config.transitions[event];
			
			(function(klazz, event) {
				klazz.prototype[event] = function() {
					//the class gets to handle the event first, however "this" inside of the event handler will be the current instance not the class instance
					var args = [event, this].concat(Array.prototype.slice.call(arguments));
					this.constructor.getClassEmitter().emit.apply(this.constructor.getClassEmitter(), args);

					//then the individual instance
					this.emit.apply(this, [event].concat(Array.prototype.slice.call(arguments)));
				};
			})(klazz, event);
			
			klazz.getClassEmitter().on(event, (function(event, transitions) {
			
				return function() {
					var args = Array.prototype.slice.call(arguments)
					  , instance = args[0];

					transitions.forEach(function(transition) {
						if (instance.getCurrentState().name != transition.from)
							return;
						var nextState = klazz.getState(transition.to);
						if (!nextState)
							return;
						if (nextState.evaluate.apply(nextState, args))
							instance.setCurrentState(nextState);
					});
				};

			})(event, transitions));
		}

	}
	*/
};
Machine.getClassEmitter = function() {
	if (!this.classEmitter) {
		this.classEmitter = new EventEmitter();
	}
	return this.classEmitter;
};
Machine.getState = function(name) {
	return this.getStates()[name];
};
Machine.addState = function(state) {
	this.getStates()[state.name] = state;
};
Machine.getStates = function() {
	if (!this.states)
		this.states = {};
	return this.states;
};
Machine.getInitialState = function() {
	if (!this.initialState)
		this.initialState = this.getState(this.config.initial) || new State(); 
	return this.initialState;
};
Machine.prototype = new EventEmitter();
Machine.prototype.constructor = Machine;
Machine.prototype.getState = function(name) {
	return this.constructor.getState(name);
};
Machine.prototype.setCurrentState = function(state) {
	if (this.currentState)
		this.currentState.exit(this);
	state.enter(this);
	this.currentState = state;
	this.emit('stateChanged');
};
Machine.prototype.getCurrentState = function() {
	if (!this.currentState)
		this.setCurrentState(this.getInitialState());
	return this.currentState;
};
Machine.prototype.getInitialState = function() {
	return this.constructor.getInitialState();
};
Machine.prototype.evaluate = function() {
	if (this.getCurrentState().evaluate(this))
		return;
	var states = this.constructor.getStates();
	for (var k in states) {
		var state = states[k];
		if (state.evaluate(this)) {
			this.setCurrentState(state);
			break;
		}
	}
};

function State(name, config) {
	EventEmitter.call(this);
	this.name = name;
	this.constructor.configure(this, config);
}
State.configure = function(state, config) {
	if (config.enter && typeof config.enter == 'function') 
		state.on('enter', config.enter);
	if (config.exit && typeof config.exit == 'function') 
		state.on('exit', config.exit);
	if (config.evaluate && typeof config.evaluate == 'function')
		state.evaluate = config.evaluate;
};
State.prototype = new EventEmitter();
State.prototype.constructor = State;
State.prototype.evaluate = function(context) { return true; }
State.prototype.enter = function(context) {
	this.emit('enter', context);
};
State.prototype.exit = function(context) {
	this.emit('exit', context);
};

function makeMachine(config) {
	var klazz = function() {
		Machine.apply(this, Array.prototype.slice.call(arguments));
	};
	klazz.prototype = new Machine();
	klazz.prototype.constructor = klazz;
	Machine.configure(klazz, config);
	return klazz;
}


var Light = makeMachine({
	init: function() {
		console.log('light created');
	},
	initial:'off',
	states: {
		off: {
			evaluate: function(c) {
				return (c.getCurrentState().name == 'on');
			},
			enter: function() {
				console.log('off');
			}
		},
		on: {
			evaluate: function(c) {
				return (c.getCurrentState().name == 'off');
			},
			enter: function() {
				console.log('on');
			}
		}
	}
});

var v = new Light();
v.evaluate();

var Bug = makeMachine({
	init: function() {
		console.log('bug created');
		this.age = 0;
	},
	initial:'egg',
	states: {
		egg: {
			evaluate: function(c) {
				return (c.age < 7);
			},
			enter: function() {
				console.log('an egg');
			}
		},
		larva: {
			evaluate: function(c) {
				return (c.age < 14);
			},
			enter: function() {
				console.log('a larva');
			}
		},
		pupa: {
			evaluate: function(c) {
				return (c.age < 21);
			},
			enter: function() {
				console.log('a pupa');
			}
		},
		adult: {
			evaluate: function(c) {
				return (c.age < 28);
			},
			enter: function() {
				console.log('adult');
			}
		},
		death: {
			evaluate: function(c) {
				return c.age > 35;
			},
			enter: function() {
				console.log('death');
			}
		}
	}
});

Bug.prototype.tick = function() {
	if (!this.last)
		this.last = new Date().getTime();
	var now = new Date().getTime();
	var delta = now - this.last;
	if (delta > 1000) {
		this.age += Math.round(delta/1000);
		this.evaluate();
		this.last = now;
	}
};
/*
var b = new Bug();
setInterval(function() {
	b.tick();
},100);
*/

exports.make = makeMachine;
