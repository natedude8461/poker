var EventEmitter = require('events').EventEmitter;

var Attributes = function(emitter) {
	this.attrs = {};
	this.emitter = emitter || { emit: function() {} }
};
Attributes.prototype.set = function(name, value) {
	var attrs = this.attrs;
	if (attrs[name] !== undefined) {
		attrs[name] = value;
		this.emitter.emit('propertySet', name, value);
	}
	else {
		var old = attrs[name];
		attrs[name] = value;
		if (old != value)
			this.emitter.emit('propertyChange', name, value, old);
	}
};
Attributes.prototype.get = function(name) {
	return this.attrs[name];
};
Attributes.prototype.has = function(name) {
	return this.attrs[name] !== undefined;
};
Attributes.prototype.hasValue = function(name, value) {
	return this.attrs[name] === value;
};

var Machine = function(context) {
	this.context = contet;
};
Machine.prototype = new EventEmitter();
Machine.prototype.when = function(name, fn) {
	if (!typeof context[name] == 'function')
		return this.on(name, this.handle);
	this.on('evaluate', function() {
		if (context[name].call(context)) {
			fn.apply(this, Array.prototype.slice.call(arguments));
		}
	});
};

Machine.prototype.handle = function() {
	console.warn('default handle for ' + Array.prototype.slice.call(arguments));
};

var table = new EventEmitter();
table.attrs = new Attributes(table);
table.set = function(key,value) {
	table.attrs.set(key, value);
};
table.get = function(key, value) {
	return this.table.attrs.get(key, value);
};
table.playersSeated = function() {
	return this.get('players').length > 1;
};
table.set('players',[]);
table.on('propertyChange', function(name, key, value) {
	game.evaluate();
});

var game = new Machine(table);
game.on('propertyChange', function(key
