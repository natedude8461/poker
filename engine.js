var poker = require('./poker');
var EventEmitter = require('events').EventEmitter;
var emitter = new EventEmitter();

emitter.emit = function() {
	console.log(arguments[0]);
	EventEmitter.prototype.emit.apply(this, Array.prototype.slice.call(arguments));
};

function Player() {
	this.tables = [];
	this.lobbies = [];
	this.hand = new poker.Hand();
}
Player.prototype.sit = function(table) {
	emitter.emit('player.sit', this, table);
	if (!this.sitting(table))
		table.seat(this);
};
Player.prototype.seated = function(table) {
	emitter.emit('player.seated', this, table);
	this.tables.push(table);
	table.seated(this);
};
Player.prototype.sitting = function(table) {
	return this.tables.indexOf(table) > -1;
};
Player.prototype.stand = function(table) {
	emitter.emit('player.stand', this, table);
	if (this.sitting(table))
		table.unseat(this);
};
Player.prototype.unseated = function(table) {
	this.tables.splice(this.tables.indexOf(table), 1);
	emitter.emit('player.unseated', this, table);
	table.unseated(this);
};
Player.prototype.delt = function(dealer, card) {
	this.hand.add(card);
	emitter.emit('player.delt', this, dealer, card);
	dealer.delt(card, this);
};
Player.prototype.fold = function() {
	emitter.emit('player.fold', this);
	this.hand = new Hand();
};
Player.prototype.check = function() {
	emitter.emit('player.check', this);
};
Player.prototype.bet = function(bet) {
	emitter.emit('player.bet', this, bet);
};
Player.prototype.join = function(lobby) {
	emitter.emit('player.join', this, lobby);
	lobby.join(this);
};
Player.prototype.joined = function(lobby) {
	this.lobbies.push(lobby);
	emitter.emit('player.joined', this, lobby);
	lobby.joined(this);
};
Player.prototype.leave = function(lobby) {
	if (this.lobbies.indexOf(lobby) < 0)
		return;
	lobby.leave(this);
};
Player.prototype.left = function(lobby) {
	this.lobbies.splice(this.lobbies.indexOf(lobby), 1);
	emitter.emit('player.left', this, lobby);
	lobby.left(player);
};

function Table() {
	this.players = [];
}
Table.prototype.seat = function(player) {
	emitter.emit('table.seat', this, player);
	if (!this.sitting(player))
		player.seated(this);
};
Table.prototype.seated = function(player) {
	this.players.push(player);
	emitter.emit('table.seated', this, player);
}
Table.prototype.sitting = function(player) {
	return this.players.indexOf(player) > -1;
};
Table.prototype.unseat = function(player) {
	emitter.emit('table.unseat', this, player);
	if (this.sitting(player))
		player.unseated(this);
};
Table.prototype.unseated = function(player) {
	this.players.splice(this.players.indexOf(player), 1);
	emitter.emit('table.unseated', this, player);
};

function Dealer() {
}
Dealer.prototype.shuffle = function(deck) {
	deck.shuffle();
	emitter.emit('dealer.shuffled', this, deck); 
};
Dealer.prototype.burn = function(deck) {
	deck.draw();
	emitter.emit('dealer.burned', this, deck);
};
Dealer.prototype.deal = function(card, player) {
	emitter.emit('dealer.deal', this, card, player);
	player.delt(this, card);
};
Dealer.prototype.delt = function(card, player) {
	emitter.emit('dealer.delt', this, card, player);
};

function Lobby() {
	this.players = [];
}
Lobby.prototype.join = function(player) {
	emitter.emit('lobby.join', this, player);
	if (this.players.indexOf(player) < 0)
		player.joined(this);
};
Lobby.prototype.joined = function(player) {
	this.players.push(player);
	emitter.emit('lobby.joined', this, player);
};
Lobby.prototype.leave = function(player) {
	emitter.emit('lobby.leave', this, player);
	if (this.players.indexOf(player) > -1)
		player.left(this);
};
Lobby.prototype.left = function(player) {
	this.players.splice(this.players.indexOf(player),1);
	emitter.emit('lobby.left', this, player);
};

function Game(dealer, deck, table) {
	this.dealer = dealer;
	this.deck = deck;
	this.table = table;
}
Game.prototype.start = function() {
	this.started = true;
	emitter.emit('game.start', this);
};
Game.prototype.shuffle = function() {
	this.dealer.shuffle(this.deck);
	this.shuffle = function() { 
		return false;
	};
};
Game.prototype.deal = function() {
	var round = 0,
		player = 0;
	this.deal = function() {
		if (round > 1) {
			this.deal = function() { 
				//TODO handle next action?
			};
			return;
		}
		if (player++ >= this.table.players.length) {
			round++;
			player = 0;
			this.deal();
			return;
		}
		var card = this.deck.draw();
		this.dealer.deal(card, this.tables.players[player]);
	};
};

emitter.on('game.start', function(game) {

	emitter.once('dealer.shuffled', function(dealer, deck) {

		emitter.on('player.delt', function(player, dealer, card) {
			
			game.deal();

		});
	
		game.deal();	

	});

	game.shuffle();

});


(function() {
	var lobby = new Lobby()
	  , dealer = new Dealer()
	  , deck = new poker.Deck()
	  , table = new Table()
	  , game = new Game(dealer, deck , table)
	  ;

	for (var i=0; i<8; i++) {
		(new Player()).join(lobby);
	}

	lobby.players.forEach(function(player) {
		player.sit(table);
	});

	game.start();

})();
