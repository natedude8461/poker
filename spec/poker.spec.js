var poker = require('../poker');

var orderedDeck = new poker.Deck();
var aceOfHearts = new poker.Card('Hearts', 'Ace');

describe('Poker', function() {

	describe('Card', function() {
		
		describe('creating the Ace of Hearts', function() {
		
			var card = new poker.Card('Hearts', 'Ace');

			it('should be the Ace of Hearts', function() {
				expect(card.equals(aceOfHearts)).toEqual(true);
			});

		});

	});

	describe('Deck', function() {
		
		var deck = new poker.Deck();

		it('should have 52 cards', function() {
			expect(deck.cards.length).toEqual(52);
		});

		(function(suits) {
			
			var cardsBySuit = {};

			suits.forEach(function(suit) {
				cardsBySuit[suit] = 0;
			});

			deck.cards.forEach(function(card) {
				cardsBySuit[card.suit]++;
			});

			suits.forEach(function(suit) {
				it('should have 13 '+suit, function() {
					expect(cardsBySuit[suit]).toEqual(13);		
				});
			});

			suits.forEach(function(suit) {
				
				(function(ranks) {	

					ranks.forEach(function(rank) {
					
						it('should have the ' + rank + ' of ' + suit, function() {
							
							var index = (suits.indexOf(suit) * ranks.length) + ranks.indexOf(rank);

							expect(deck.cards[index].rank == rank && deck.cards[index].suit == suit).toEqual(true); 

						});

					});

				})('Two Three Four Five Six Seven Eight Nine Ten Jack Queen King Ace'.split(' '));

			});

		})('Hearts Spades Diamonds Clubs'.split(' '))

		it('should not be ordered after shuffling', function() {
			deck.shuffle();
			var ordered = true;
			for (var i=0; i<deck.cards.length; i++) {
				if (deck.cards[i].rank == orderedDeck.cards[i].rank &&
					deck.cards[i].suit == orderedDeck.cards[i].suit) {
					continue;
				}
				ordered = false;
				break;
			}
			expect(ordered).toEqual(false);
		});

		it('should have 51 cards after drawing one', function() {
			deck.draw();
			expect(deck.cards.length).toEqual(51);
		});
		
	});


	describe('Hand', function() {

		describe('drawing two cards from the deck', function() {
			var deck = new poker.Deck();
			deck.shuffle();

			var hand = new poker.Hand();
			hand.add(deck.draw());
			hand.add(deck.draw());
			
			it('should have 2 cards', function() {
				expect(hand.size()).toEqual(2)
			});
		});
		
		describe('getHighCard()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Two'));
			hand.add(new poker.Card('Diamonds', 'Three'));

			it('should get the card Ace of Hearts', function() {
				expect(hand.getHighCard().shift()).toEqual(hand.cards[hand.cards.length-1]);
			});
		});
		
		describe('getPair()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));

			it('should get a pair', function() {
				expect(hand.getPair().getCards()).toEqual(hand.cards);
			});
		});

		
		describe('a hand having a pair', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));

			it('should be recognized as having a pair', function() {
				expect(hand.hasPair()).toEqual(true);
			});
		});
		
		describe('getTwoPair()', function() {
			var hand = new poker.Hand();
			var cards = [
				new poker.Card('Hearts', 'King'),
				new poker.Card('Diamonds', 'King'),
				new poker.Card('Hearts', 'Ace'),
				new poker.Card('Diamonds', 'Ace')
			];
			cards.forEach(function(card) {
				hand.add(card);
			});
			it('should get two pair', function() {
				expect(hand.getTwoPair().getCards()).toEqual(cards);
			});
		});

		describe('a hand having two pair', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Hearts', 'King'));
			hand.add(new poker.Card('Diamonds', 'King'));

			it('should be recognized as having two  pair', function() {
				expect(hand.hasTwoPair()).toEqual(true);
			});
		});
		
		describe('getThreeOFAKind()', function() {
			var hand = new poker.Hand();
			var cards = [
				new poker.Card('Hearts', 'Ace'),
				new poker.Card('Diamonds', 'Ace'),
				new poker.Card('Clubs', 'Ace')
			];
			cards.forEach(function(card) {
				hand.add(card);
			});

			it('should be get three-of-a-kind', function() {
				expect(hand.getThreeOfAKind().getCards()).toEqual(cards);
			});
		});
		
		describe('a hand having three-of-a-kind', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Ace'));

			it('should be recognized as having three-of-a-kind', function() {
				expect(hand.hasThreeOfAKind()).toEqual(true);
			});
		});

		describe('getStraight()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Two'));
			hand.add(new poker.Card('Diamonds', 'Three'));
			hand.add(new poker.Card('Clubs', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Spades', 'Six'));

			it('should get a straight', function() {
				expect(hand.getStraight().getCards()).toEqual(hand.cards);
			});
		});

		describe('a hand having a straight', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Two'));
			hand.add(new poker.Card('Diamonds', 'Three'));
			hand.add(new poker.Card('Clubs', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Spades', 'Six'));

			it('should be recognized as having a straight', function() {
				expect(hand.hasStraight()).toEqual(true);
			});
		});

		describe('getFlush()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Two'));
			hand.add(new poker.Card('Hearts', 'Three'));
			hand.add(new poker.Card('Hearts', 'King'));
			hand.add(new poker.Card('Hearts', 'Queen'));
			hand.add(new poker.Card('Hearts', 'Ten'));

			it('should get a flush', function() {
				expect(hand.getFlush().getCards()).toEqual(hand.cards);
			});
		});

		describe('a hand having a flush', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Two'));
			hand.add(new poker.Card('Hearts', 'Three'));
			hand.add(new poker.Card('Hearts', 'King'));
			hand.add(new poker.Card('Hearts', 'Queen'));
			hand.add(new poker.Card('Hearts', 'Ten'));

			it('should be recognized as having a flush', function() {
				expect(hand.hasFlush()).toEqual(true);
			});
		});
		
		describe('getFullHouse()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Hearts', 'King'));
			hand.add(new poker.Card('Clubs', 'King'));

			it('should get a full house', function() {
				expect(hand.getFullHouse().getCards()).toEqual(hand.getThreeOfAKind().concat(hand.getPair()));
			});
		});

		describe('a hand having a full house', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Hearts', 'King'));
			hand.add(new poker.Card('Clubs', 'King'));

			it('should be recognized as having a full house', function() {
				expect(hand.hasFullHouse()).toEqual(true);
			});
		});
		
		describe('getFourOfAKind()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Spades', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Ace'));

			it('should get four-of-a-kind', function() {
				expect(hand.getFourOfAKind().getCards()).toEqual(hand.cards);
			});
		});
		
		describe('a hand having four-of-a-kind', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Hearts', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Spades', 'Ace'));

			it('should be recognized as having four-of-a-kind', function() {
				expect(hand.hasFourOfAKind()).toEqual(true);
			});
		});
		
		describe('getStraightFlush()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Two'));
			hand.add(new poker.Card('Clubs', 'Three'));
			hand.add(new poker.Card('Clubs', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Clubs', 'Six'));
			
			it('should be recognized as having a straight flush', function() {
				expect(hand.getStraightFlush()).toEqual(hand.cards);
			});
		});

		describe('a hand having a staright flush', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Two'));
			hand.add(new poker.Card('Clubs', 'Three'));
			hand.add(new poker.Card('Clubs', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Clubs', 'Six'));
			
			it('should be recognized as having a straight flush', function() {
				expect(hand.hasStraightFlush()).toEqual(true);
			});
		});
		
		describe('getRoyalFlush()', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Ten'));
			hand.add(new poker.Card('Clubs', 'Jack'));
			hand.add(new poker.Card('Clubs', 'King'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Queen'));

			it('should be recognized as having a royal flush', function() {
				expect(hand.getRoyalFlush()).toEqual(hand.cards);
			});

			it('should have an ace', function() {
				expect(hand.hasAce()).toEqual(true);
			});
		});

		describe('a hand having a royal flush', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Ten'));
			hand.add(new poker.Card('Clubs', 'Jack'));
			hand.add(new poker.Card('Clubs', 'King'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Clubs', 'Queen'));

			it('should be recognized as having a royal flush', function() {
				expect(hand.hasRoyalFlush()).toEqual(true);
			});

			it('should have an ace', function() {
				expect(hand.hasAce()).toEqual(true);
			});
		});


		describe('a hand being a low straight', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Two'));
			hand.add(new poker.Card('Clubs', 'Three'));
			hand.add(new poker.Card('Diamonds', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			
			it('should be recognized as having a low straight', function() {
				expect(hand.hasStraight()).toEqual(true);
			});
			
			it('should have an ace', function() {
				expect(hand.hasAce()).toEqual(true);
			});

		});
		
		describe('a hand being a low straight flush', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Two'));
			hand.add(new poker.Card('Clubs', 'Three'));
			hand.add(new poker.Card('Clubs', 'Four'));
			hand.add(new poker.Card('Clubs', 'Five'));
			hand.add(new poker.Card('Clubs', 'Ace'));
			
			it('should be recognized as having a low straight flush', function() {
				expect(hand.hasStraightFlush()).toEqual(true);
			});
			
			it('should have an ace', function() {
				expect(hand.hasAce()).toEqual(true);
			});

		});

		describe('a hand with 7 cards but getting the best 5', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Two'));
			hand.add(new poker.Card('Hearts', 'Queen'));
			hand.add(new poker.Card('Hearts', 'Seven'));
			hand.add(new poker.Card('Spades', 'Three'));
			hand.add(new poker.Card('Clubs', 'Nine'));
			hand.add(new poker.Card('Diamonds', 'Three'));
			
			it('should be Three of Diamonds, Three of Spades, Ace of Clubs, Queen of Hearts, Nine of Clubs', function() {
				var cards = [ 
					new poker.Card('Spades', 'Three'),
					new poker.Card('Diamonds', 'Three'), 
					new poker.Card('Clubs', 'Ace'),
					new poker.Card('Hearts', 'Queen'),
					new poker.Card('Clubs', 'Nine')
				];
				expect(hand.makeBest().getCards()).toEqual(cards);
			});

		});

		describe('a hand with two pair and a high card', function() {
			var hand = new poker.Hand();
			hand.add(new poker.Card('Clubs', 'Ace'));
			hand.add(new poker.Card('Diamonds', 'Ace'));
			hand.add(new poker.Card('Clubs', 'King'));
			hand.add(new poker.Card('Diamonds', 'King'));
			hand.add(new poker.Card('Hearts', 'Queen'));
			

			it('should have a rank of TwoPair', function() {
				expect(hand.getRank()).toEqual('TwoPair');
			});

			it('should have a score of 2056', function() {
				expect(hand.getScore()).toEqual(2056);
			});
			
		});

		describe('two hands', function() {
			var a = new poker.Hand();
			a.add(new poker.Card('Clubs', 'Ace'));
			a.add(new poker.Card('Diamonds', 'Ace'));
			a.add(new poker.Card('Clubs', 'King'));
			a.add(new poker.Card('Diamonds', 'King'));
			a.add(new poker.Card('Hearts', 'Queen'));
			var b = new poker.Hand();
			b.add(new poker.Card('Clubs', 'Ace'));
			b.add(new poker.Card('Diamonds', 'Ace'));
			b.add(new poker.Card('Clubs', 'King'));
			b.add(new poker.Card('Diamonds', 'King'));
			b.add(new poker.Card('Hearts', 'Jack'));
			it('a should be better than b', function() {
				expect(a.betterThan(b)).toEqual(true);
			});

		});

	});

});
