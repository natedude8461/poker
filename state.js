function Context() {
}
Context.prototype.enter = function(state) {
	if (this.state) {
		this.state.exit(this);
	}
	state.enter(this);
	this.state = state;
};
Context.prototype.evaluate = function(v) {
	var self = this;
	var currentState = this.getCurrentState();
	var nextPossibleStates = this.getNextPossibleStates(currentState);
	var passingPossibleStates= [];
	if (v) {
		for (var i=0; i<nextPossibleStates.length; i++) {
			var nextPossibleState = nextPossibleStates[i];
			if ((v.to.indexOf ? v.to.indexOf(nextPossibleState.name) > -1 : (nextPossibleState.name = v.to || v.to == '*')) || v.to == '*') {
				if (nextPossibleState.evaluate(this)) {
					passingPossibleStates.push(nextPossibleState);
				}
			}
		}
	}
	else {
		for (var i=0; i<nextPossibleStates.length; i++) {
			var nextPossibleState = nextPossibleStates[i];
			if (nextPossibleState.evaluate(this)) {
				passingPossibleStates.push(nextPossibleState);
			}
		}
	}
	console.log('passingPossibleStates',passingPossibleStates);
	passingPossibleStates.forEach(function(state) {
		self.enter(state);
	});
};
Context.prototype.getNextPossibleStates = function(state) {
	var self = this
	  , config = self.constructor.config
	  , events = config.events || {}
	  , to = []
	  ;
	
	for (var k in events) {
		var v = events[k];
		if ((v.from.indexOf ? v.from.indexOf(state.name) > -1 : v.from == state.name) || v.from == '*') {
			var check = v.to.forEach ? v.to : [v.to];
			if (check[0] == '*') {
				for (var j in config.states) {
					var s = self.getState(j);
					if (to.indexOf(s) < 0)
						to.push(s);
				}
			}
			else {
				check.forEach(function(c) {
					var s = self.getState(c);
					if (to.indexOf(s) < 0)
						to.push(s);
				});
			}
		}
	}

	if (!to.length) {
		to.push(state);
	}
	return to;
};
Context.prototype.getNextState = function() {
	return this.getNextStateFor(this.getCurrentState());
};
Context.prototype.getNextStateFor = function(state) {
	var currStateName = this.getCurrentState().name
	  , config = this.constructor.config
	  , events = config.events || {}
	  , to = []
	  , k = null
	  , v = null
	  ;
	for (k in events) {
		v = events[k];
		if (v.from == currStateName) {
			if (to.indexOf(v.from) < 0)
				to.push(v.from);
		}
	}
	
	//just return the first state, TODO support evaluating all possible to states
	var nextStateFor = this.getState(to.shift()) || this.getState(config.initial);
	return nextStateFor;
};
Context.prototype.getState = function(name) {
	var states = this.getStates();
	if (name in states)
		return states[name];
	return Context.getInitialState();
};
Context.prototype.addState = function(state) {
	var states = this.getStates();
	if (state.name in states)
		return;
	states[state.name] = state;
};
Context.prototype.getStates = function() {
	if (!this.states) {
		this.states = {};
	}
	return this.states;
};
Context.prototype.getCurrentState = function() {
	if (!this.state) {
		//this.state = Context.getInitialState();
		this.state = this.getState(this.constructor.config.initial) || Context.getInitialState();
	}
	return this.state;
};
Context.getInitialState = function() {
	if (!this.initialState) {
		this.initialState = new State('initial');
	}
	return this.initialState;
};
Context.make = function(config) {
	var context = new Context()
	  , k = null
	  , v = null
	  ;

	this.configure(context);

	return context;
};
Context.configure = function(context, config) {
	if (config.states) {
		for (k in config.states) {
			v = config.states[k];
			if (!v)
				throw new Error('no state config for ' + k );
			context.addState(new State(
				k,
				(typeof v.evaluation == 'function' ? v.evaluation : undefined),
				(typeof v.enter == 'function' ? v.enter : undefined),
				(typeof v.exit == 'function' ? v.exit : undefined)
			));
		}
	}

	if (config.events) {
		for (k in config.events) {
			v = config.events[k];
			context[k] = (function(k,v) {
				return function() {
					this.evaluate(v);
				};
			})(k,v);
		}
	}

	//context.enter( context.getState(config.initial) );
};
Context.makeClass = function(config) {
	var Klazz = function() {
		Context.call(this);
		if (typeof config.init == 'function')
			config.init.call(this);
	};
	Klazz.config = config;
	Klazz.prototype = new Context();
	Klazz.prototype.constructor = Klazz;
	Context.configure(Klazz.prototype, config);
	return Klazz;
};

function State(name, evaluation, enter, exit) {
	if (name && typeof name == 'string')
		this.name = name;
	else
		throw new Error('the tate needs a name');
	if (evaluation && typeof evaluation == 'function')
		this.evaluation = evaluation;
	if (enter && typeof enter == 'function')
		this.enter = enter;
	if (exit && typeof exit == 'function')
		this.exit = exit;
}
State.prototype.evaluation = function(context) { return true; }
State.prototype.evaluate = function(context) {
	return this.evaluation(context);
};
State.prototype.enter = function(context) {};
State.prototype.exit = function(context) {};

var Light = Context.makeClass({
	init: function() {
		console.log('light created');
	},
	initial: 'off',
	events: {
		on: { from: 'off', to: 'on' },
		off: { from: 'on', to: 'off' }
	},
	states: {
		on: {
			evaluation: function(context) {
				console.log('check if we should turn on');
				return (context.getCurrentState().name == 'off');
			},
			enter: function(context) {
				console.log('light on');
			}
		},
		off: {
			evaluation: function(context) {
				console.log('check if we should turn off');
				return (context.getCurrentState().name == 'on');
			},
			enter: function(context) {
				console.log('light off');
			}
		}
	}
});

var light = new Light();
light.on();
light.off();


var Toggle = Context.makeClass({
	init: function() {
		console.log('toggle created');
	},
	initial:'a',
	events: {
		toggle: { from: '*', to: '*' }
	},
	states: {
		a: {
			evaluation: function(context) {
				console.log('checking if we should toggle to a');
				return (context.getCurrentState().name == 'b');
			},
			enter: function(context) {
				console.log('on a');
			}
		},
		b: {
			evaluation: function(context) {
				console.log('checking if we should toggle to b');
				return (context.getCurrentState().name == 'a');
			},
			enter: function(context) {
				console.log('on b');
			}

		}
	}
});
var toggle = new Toggle();
toggle.toggle();
toggle.toggle();


var Character = Context.makeClass({
	init: function() {
		console.log('character created');
		this.energy = 50;
	},
	initial:'idle',
	events: {
		idle: { from: '*', to: '*' },
		invade: { from: '*', to: '*' },
		evade: { from: '*', to: '*' }
	},
	states: {
		idle: {
			evaluation: function(context) {
				console.log('checking if we should idle', context.energy);
				return context.energy == 50;
			},
			enter: function(context) {
				console.log('idleing...');
			}
		},
		invade: {
			evaluation: function(context) {
				console.log('checking if we should invade', context.energy);
				return context.energy > 50;
			},
			enter: function(context) {
				console.log('invading...');
			}
		},
		evade: {
			evaluation: function(context) {
				console.log('checking if we should evade ', context.energy);
				return context.energy < 50;
			},
			enter: function(context) {
				console.log('evading...');
			}
		}
	}
});
Character.prototype.tick = function() {
	var d = (Math.round(Math.random(1)) ? 1 : -1);
	var m = (Math.round(Math.random(10)))
	this.energy +=  d * m ;
	this.evaluate();
}
var c = new Character();
c.tick();
c.tick();
c.tick();
c.tick();
c.tick();
c.tick();


var TrafficLight = Context.makeClass({
	init: function() {
		console.log('traffic light created');
	},
	initial: 'red',
	events: {
		red: { from:'yellow', to:'red' },
		yellow: { from:'green', to:'yellow' },
		green: { from:'red', to:'green' }
	},
	states: {
		red: {
			enter: function() {
				console.log('stop');
			}
		},
		green: {
			enter: function() {
				console.log('go');
			}
		},
		yellow: {
			enter: function() {
				console.log('slow');
			}
		}
	}

});
var trafficLight = new TrafficLight();
trafficLight.evaluate();
trafficLight.evaluate();
trafficLight.evaluate();


var Bug = Context.makeClass({
	init: function() {
		console.log('bug created');
		this.health = Math.round(Math.random(100));
		this.energy = Math.round(Math.random(100));
		this.id = (++this.constructor.id);
	},
	initial:'idle',
	events: {

	},
	states: {
		idle: {
			evaluation: function(context) {
				console.log('check idle'+context.id);
				return context.energy > 0 && context.health < Math.round(Math.random(30))+15;
			},
			enter: function(context) {
				console.log('idle'+context.id);
				context.energy--;
				context.health++;
			}
		},
		dying: {
			evaluation: function(context) {
				console.log('check dying'+context.id);
				return (context.health < Math.round(Math.random(15)));
			},
			enter: function(context) {
				console.log('dying'+context.id);
				context.health-=2;
				context.energy++;
			}
		},
		dead: {
			evaluation: function(context) {
				console.log('check dead'+context.id);
				return context.health <= 0;
			},
			enter: function(context) {
				console.log('dead'+context.id);
			}
		},
		forage: {
			evaluation: function(context) {
				console.log('check forage'+context.id);
				return context.energy < 100;
			},
			enter: function(context) {
				console.log('forage'+context.id);
				context.energy++;
				context.health--;
			}
		},
		attack: {
			evaluation: function(context) {
				console.log('check attack'+context.id);
				return (context.energy > Math.round(Math.random(40)) + 40) && (context.health > Math.round(Math.random(20)) + 80);
			},
			enter: function(context) {
				console.log('attack'+context.id);
				context.energy--;
			}
		},
		evade: {
			evaluation: function(context) {
				console.log('check evade'+context.id);
				return !(context.energy > Math.round(Math.random(40)) + 40) && (context.health > Math.round(Math.random(20)) + 80);
			},
			enter: function(context) {
				console.log('evade'+context.id);
				context.energy--;
			}
		}

	}
});
Bug.id = 0;
(function() {
	var bug = new Bug();
	setInterval(function() {
		bug.evaluate();
	},500);
})();

(function() {
	var bug = new Bug();
	setInterval(function() {
		bug.evaluate();
	},500);
})();

(function() {
	var bug = new Bug();
	setInterval(function() {
		bug.evaluate();
	},500);
})();
